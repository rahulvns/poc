import { createStackNavigator } from 'react-navigation';
import Login from './screen/Login';

const AppNavigator = createStackNavigator({
    Login: { screen: Login },
});

export default AppNavigator;