import React, {Component} from 'react';
import { 
  Platform, StyleSheet, Text, View, Image, ImageBackground,
  ActivityIndicator, TouchableOpacity, AsyncStorage
} from 'react-native';

import { Actions } from 'react-native-router-flux';



export default class Splash extends Component{

  constructor(props) {
    super(props);
    this.state = {
      flag: 1,
    }
   
  }

  async isLoggedIn() {
  //   const vendorProfile = JSON.parse( await AsyncStorage.getItem(Constant.VENDOR_PROFILE) );
  //   console.log(vendorProfile);
  //   if(!vendorProfile === false){
  //     Actions.VendorHomePage();
  //     return;
  //   }
  //   const riderProfile = JSON.parse( await AsyncStorage.getItem(Constant.RIDER_PROFILE) );
  //   console.log(riderProfile);
  //   if(!riderProfile === false){
  //     Actions.RiderHome();
  //     return;
  //   }

  //   Actions.Login({ userType: Constant.VENDOR });
  // }

  }
  componentDidMount() {
    //alert('entro!')
    
    
}

  subContainer1(){
    // alert('1');
    return (
      <ImageBackground style={styles.image} source={require('../public/image/bgimage.jpg')} >
        <View style={styles.subContainer1}>
       
           
           <View style = {{flexDirection: 'row', justifyContent:'space-between', alignItems: 'center' }} >
             <TouchableOpacity style = {[styles.smallButton, { backgroundColor: 'red'}]} onPress = {()=>{Actions.Login()}}>
               <Text style = {{color: 'white', fontWeight: '500' }} >Grower</Text>
             </TouchableOpacity>
             <TouchableOpacity style = {[styles.smallButton, { backgroundColor: 'black'}]} onPress = { ()=>{Actions.TruckerLogin()} } >
               <Text style = {{color: 'white', fontWeight: '500' }} >Trucker</Text>
             </TouchableOpacity>
             </View>
           
        </View>
      </ImageBackground>
    );
  }
  subContainer2(){
    // alert('2');
    return (
      <ImageBackground style={styles.image} source={require('../public/image/splash.jpg')} >
        <View style={styles.subContainer2}>
          
          <View style={styles.subContainer3} >
           
            <View style = {{flexDirection: 'row', justifyContent:'space-between', alignItems: 'center' }} >
              <TouchableOpacity style = {[styles.smallButton, { backgroundColor: 'red'}]} onPress = {()=>{Actions.Login()}}>
                <Text style = {{color: 'white', fontWeight: '500' }} >Grower</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {[styles.smallButton, { backgroundColor: 'black'}]} onPress = { ()=>{Actions.TruckLogin()()} } >
                <Text style = {{color: 'white', fontWeight: '500' }} >Trucker</Text>
              </TouchableOpacity>
            </View>
           
          </View>
      </View>
      </ImageBackground>
    );
  }


  render() {
    const { flag } = this.state;
    console.log('flag : ', flag);
    return this.subContainer1();
  }
}

const styles = StyleSheet.create({
  logo: {
    height: '20%',
    width: '100%',
    resizeMode: 'center',
  },
  image: {
    height: '100%',
    width: '100%',
    resizeMode: 'cover',
    alignItems: 'center',
    justifyContent:'center',
  },
  subContainer1: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  subContainer2: {
    height: '80%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  subContainer3: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255,0.6)',
    marginTop: 30,
    padding: 20,
    borderRadius:10
  },
  smallButton: {
    width: 120,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent:'center',
    borderRadius:5,
    marginHorizontal: 15,
  }
})