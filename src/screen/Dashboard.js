/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {Platform, Button,StyleSheet, Text, View, TouchableOpacity, ProgressBarAndroid, ProgressViewIOS,Dimensions} from 'react-native';
import {
  Container, Header, Content, Card, CardItem, Body, Form, Textarea,Icon, Right, ActionSheet, 
} from 'native-base';

import { Actions } from 'react-native-router-flux';
// const win = Dimensions.get('window');
export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false,
      dimensions: undefined
     
    }
    
  }
  onPress = () => {
    alert('hi');
  }
  onLayout = event => {
    if (this.state.dimensions) return // layout was already called
    let {width, height} = event.nativeEvent.layout
    this.setState({dimensions: {width, height}})
  }
  render() {
    if (this.state.dimensions) {
      var { dimensions } = this.state
      var { width, height } = dimensions
      
    }
    return (
      <Container>
        <Content>
          <View style = {styles.container} onLayout={this.onLayout}>
             <View style = {styles.progressView}>
             <Text style = {{fontSize: 24, color: '#15DEC3',}}> Inventory Status: { parseFloat((this.state.Progress_Value * 100).toFixed(3))} 2000</Text>
            <View style = {{height:10, marginTop:20}}>
             {( Platform.OS === 'android' )
                    ?
                    ( <ProgressBarAndroid progressTintColor="#4a90e2" progress = { this.state.progress } styleAttr = "Horizontal" indeterminate = { false } /> )
                    :
                    ( <ProgressViewIOS style={{transform: [{ scaleX: 1.0 }, { scaleY: 4}], height:12}} progressTintColor="#4a90e2" progress = { this.state.progress } /> )
                }
             </View>
             </View> 
           <View style={{flexDirection: 'row',height: 420,backgroundColor: '#fff',marginLeft:10,
                          marginRight:10,borderRadius: 5,borderWidth: 1,borderColor: '#000',}}>
              <View style={{width: 180, height: 200, backgroundColor: 'transparent',}}>
                  <Text style={{ color: 'red', fontSize: 14, fontWeight: '700', padding:10}} >Customer 1</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Sub-order 121-1</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Lot# 123456</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Recker 11:00AM</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Dock 1, Location</Text>
                  <Text style={{ backgroundColor:'transparent', color: 'transparent', fontSize: 12, fontWeight: '700', padding:10,borderRadius: 5,borderWidth: 1,borderColor: 'transparent',textAlign:'center'}} ></Text>
                  <View style = {styles.lineStyle} />
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Sub-order 121-2</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Lot# 123456</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Recker 11:00AM</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Dock 1, Location</Text>
              
              </View>
              <View style={{width: 180, height: 200, backgroundColor: 'transparent',}}>
                  <Text style={{ color: 'red', fontSize: 14, fontWeight: '700', padding:10}} >Order No. 121</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Quantity: 500</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Truck1</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Feb 25, 2019</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Feb 26, 2019</Text>
                  <Text style={{ backgroundColor:'#4a90e2', color: '#000', fontSize: 12, fontWeight: '700', padding:10,borderRadius: 5,borderWidth: 1,borderColor: '#000',textAlign:'center'}} >Loading</Text>
                  <View style = {styles.lineStyle} />
              
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Quantity: 500</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Truck1</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Feb 25, 2019</Text>
                  <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Feb 26, 2019</Text>
                  <Text style={{ backgroundColor:'#FFA933', color: '#000', fontSize: 12, fontWeight: '700', padding:10,borderRadius: 5,borderWidth: 1,borderColor: '#000',textAlign:'center'}} >In-Transit</Text>
              </View>
             </View>
             <View style={{flexDirection: 'row',backgroundColor: '#fff',marginLeft:10,marginRight:10,marginTop:20,justifyContent: 'flex-start',
                          alignItems: 'center',}}>
                           <Text style={{ color: '#000', fontSize: 12, fontWeight: '700', padding:10}} >Truck 1 Lot# 123456</Text>
                           {/* <Button
                             style={{borderRadius: 5,backgroundColor: 'green',borderWidth: 1,borderColor: '#000',textAlign:'center'}}
                              title="Scheduled"
                              color="#841584"
                              accessibilityLabel="hi"
                            /> */}
                            <TouchableOpacity
                                style={{borderRadius: 5,backgroundColor: 'green',borderWidth: 1,borderColor: '#000',alignItems: 'center',
                                padding: 10}}
                                onPress={this.onPress}
                              >
                                <Text> Touch Here </Text>
                              </TouchableOpacity>
                              <Right>
                              <TouchableOpacity
                               onPress={() => {Actions.GMaps()}}>
                              <Icon type="FontAwesome" name="map-marker" />
                              </TouchableOpacity>
                              </Right>
             </View>
            </View>
            </Content>
        </Container>
     
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  progressView:{
    flexDirection: 'column', 
    height: 120,
    backgroundColor: '#fff',    
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#000',
    padding:5,
    marginLeft:10,
    marginRight:10
    
  },
  customerView:{
    flexDirection: 'column', 
    height: 320,
    backgroundColor: 'red',    
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#000',
    padding:5,
    marginLeft:10,
    marginRight:10,
    marginTop:20
    
  },
  progressBar: {
    width: 100,
    transform: [{ scaleX: 1.0 }, { scaleY: 2.5 }],
  },
  lineStyle:{
    borderWidth: 0.5,
    borderColor:'black',
    margin:10,
}
});
