// import React, { Component } from 'react';
// import { Dimensions, StyleSheet,View } from 'react-native';
// import MapView from 'react-native-maps';
// import MapViewDirections from 'react-native-maps-directions';
// //import MapViewer from 'react-native-map-direction'; 
// const { width, height } = Dimensions.get('window');
// const ASPECT_RATIO = width / height;
// const LATITUDE = 37.771707;
// const LONGITUDE = -122.4053769;
// const LATITUDE_DELTA = 0.0922;
// const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
 
// const GOOGLE_MAPS_APIKEY = 'AIzaSyAwV8cPtKQnnXT8eMz1a4jjDlB1OvlLRyM';
 
// export default class GMaps extends Component {
//   constructor(props) { 
//     super(props); 
//     this.state = { 
//     geoCode: { 
//       startLoc: { 
//         lat: 8.192738, 
//         lon: -77.714723 
//       }, 
//       destinationLoc: { 
//         lat: 70.196917, 
//         lon: -148.419491 
//       } 
  
//     },   screenDimension: { 
//       width: Dimensions.get('window').width, // width 
//       height: Dimensions.get('window').height	//height 
//     }, 
//     pinColors: { 
//       start: 'green', 
//       destination: 'red' 
//     }, 
//     polyline: { 
//       strokeColor: 'navy', 
//       strokeWidth: 4 
//     } 
//     } 
//     } 
//     render() { 
//       return (
//       <MapViewDirections 
//         geoCode={this.state.geoCode} 
//         screenDimension={this.state.screenDimension} 
//         pinColors={this.state.pinColors} 
//         polyline={this.state.polyline} 
//         /> 
//       ) 
//     }; 
//   } 
 
import React, { Component } from 'react';
import { Dimensions, StyleSheet,Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert} from 'react-native';

import { OpenMapDirections } from 'react-native-navigation-directions';

export default class GMaps extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      error: null,
    };
  }
 _callShowDirections = () => {
    const startPoint = {
      longitude: -8.945406,
      latitude: 38.575078
    } 

    const endPoint = {
      longitude: -8.9454275,
      latitude: 38.5722429
    }

		const transportPlan = 'w';

    OpenMapDirections(startPoint, endPoint, transportPlan).then(res => {
      console.log(res)
    });
  }
  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }
  render() {
    return (
      <View style={styles.container}>
      <Text>Latitude: {this.state.latitude}</Text>
        <Text>Longitude: {this.state.longitude}</Text>
        {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
        <Text>Location:</Text>
        <Button
        onPress={() => { this._callShowDirections() }}
        title="Open map"
        color="#841584"
      />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#808080',
  },
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
      
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: "#00b5ec",
  },
  loginText: {
    color: 'white',
  }
});