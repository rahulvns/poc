import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert, Dimensions,
  ActivityIndicator, TouchableOpacity, ScrollView, AsyncStorage,
  Modal,
} from 'react-native';
import { Container, Content, Header, Left, Body, Right, Button, Icon, Title, Text, Form } from 'native-base';

import { Actions } from 'react-native-router-flux';

const { height, width } = Dimensions.get('window');

export default class Inventry extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      password: '',
      confirmPassword: '',
      vehicleNumber: '',
      address: '',
      animating: false,
      buttonAnimation: false
    }
  }

  componentDidMount() {
    
   
  }

  
  
  
  inputFields() {
    
    return (
      <Content >
        <Form style = {{ alignItems: 'center', marginTop:15 }} >
          <View style={styles.inputContainer}>
            
            <TextInput style={styles.inputs}
              placeholder="Name"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              placeholderTextColor='black'
           
            />
          </View>

          <View style={styles.inputContainer}>
           
            <TextInput style={styles.inputs}
              placeholder="Phone"
              keyboardType="phone-pad"
              underlineColorAndroid='transparent'
              placeholderTextColor='black'
              
            />
          </View>

          <View style={styles.inputContainer}>
            {/* <Text style = {{ fontWeight:'700', marginBottom:5 }} >Password</Text> */}
            <TextInput style={styles.inputs}
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              placeholderTextColor='black'
              
            />
          </View>
          <View style={styles.inputContainer}>
            {/* <Text style = {{ fontWeight:'700', marginBottom:5 }} >Confirm Password</Text> */}
            <TextInput style={styles.inputs}
              placeholder="Confirm Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              placeholderTextColor='black'
              
            />
          </View>

          <View style={styles.inputContainer}>
            {/* <Text style = {{ fontWeight:'700', marginBottom:5 }} >Vehicle Number</Text> */}
            <TextInput style={styles.inputs}
              placeholder="Vehicle Number"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              placeholderTextColor='black'
             
            />
          </View>

          <View style={styles.inputContainer}>
            {/* <Text style = {{ fontWeight:'700', marginBottom:5 }} >Address</Text> */}
            <TextInput style={styles.inputs}
              placeholder="Address"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              placeholderTextColor='black'
              
            />
          </View>

          <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => { this.alert() }}>
           
            <Text style={styles.signUpText}>Submit</Text>}
          </TouchableHighlight>
        </Form>
      </Content>
      // <Container>

      // </Container>
    );
  }

  render() {
    
    return (
      <Container contentContainerStyle={{ flex: 1, backgroundColor: '#ffffff' }}>
       
        {this.inputFields()}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginTop: 20,
  },
  inputContainer: {
    // flexDirection:'row',
    height: 45,
    justifyContent: 'space-between',
    width: width - 80,
    marginBottom: 20,
    marginHorizontal: 20,
    backgroundColor: '#fff'
  },
  inputs: {
    height: 42,
    width: width - 80,
    paddingHorizontal: 16,
    flex: 1,
    borderRadius: 10,
    color: '#000',
    borderRadius: 10,
    backgroundColor: '#d6d6d6',

  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center'
  },
  buttonContainer: {
    marginTop: 10,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 200,
    borderRadius: 10,
  },
  signupButton: {
    backgroundColor: "#B93B35",
  },
  signUpText: {
    color: 'white',
  }
});