import React from 'react';
import { View, StyleSheet, TouchableOpacity, AsyncStorage, Alert } from 'react-native';
import {
  Container, List, Content, Thumbnail,
  ListItem,
  Icon,
  Text,
} from 'native-base'

import { Actions } from 'react-native-router-flux';

export default class SideMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      userMobile: '',
      profilePic: 'no_image'
    }
  };

  async componentDidMount() {
   
  }


  render() {
    const { userName, userMobile, profilePic } = this.state;
    const name = (!userName) ? 'User Name' : userName;
    const mobile = (!userMobile) ? '+91-**********' : userMobile;
    let source = '';
    try {
			source = (profilePic.indexOf('no_image') >= 0 )?require('../public/icons/username.png'):{ uri: profilePic }
		} catch (err) {
			console.log(err);
    }
    console.log(source);
    return (
      <Container>
        <Content style={{ backgroundColor: '#B93B35' }}>
          <List style={{ marginTop: 40 }}>
            <View style={{ marginBottom: 10, flexDirection: 'row', padding: 20 }}>
              <View style = {{ borderWidth: 0.5, justifyContent:"center", alignItems:"center", borderColor:'#000', borderRadius:30 }} >
                {/* <Thumbnail source={source} /> */}
              </View>
              <View style={{ marginLeft: 18, justifyContent: 'flex-start', justifyContent: 'center', }}>
                <Text style={{ color: '#ffffff', fontSize: 18, textAlign: 'left' }}>Rahul</Text>
                <Text style={{ color: '#ffffff', fontSize: 14, marginTop: 8 }}>9560076199</Text>
              </View>
            </View>
            <ListItem iconLeft >
              <Text style={{ marginLeft: 10, color: '#ffffff' }}>Menu</Text>
            </ListItem>

            <TouchableOpacity iconLeft onPress = {() => { Actions.Dashboard() }} style={{ flexDirection: 'row', padding: 13, paddingLeft: 33 }}  >
              <Icon name="cart" style={{ color: '#ffffff', fontSize: 20 }} />
              <Text style={{ marginLeft: 10, color: 'white' }}>Order List</Text>
            </TouchableOpacity>

           

          </List>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
    paddingRight: 20,
    paddingLeft: 20,
    paddingBottom: 10,
  },

});