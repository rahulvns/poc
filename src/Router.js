import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Login from './screen/Login'
import Dashboard from './screen/Dashboard';
import GMaps from './screen/GMaps';
import Locations from './screen/Location';
import Splash from './screen/Splash';
import TruckerLogin from './TruckerModule/screen/TruckerLogin';
import Inventry from './screen/Inventry';

const Routes = () => (
   <Router>
      <Scene key = "root">
         <Scene key = "Splash" component = {Splash} title = "POC" />
         <Scene key = "Login" component = {Login} title = "Login" />
         <Scene key = "Dashboard" component = {Dashboard} title = "Dashboard"/>
         <Scene key = "GMaps" component = {GMaps} title = "GMaps"  />
         <Scene key = "Locations" component = {Locations} title = "Locations"  />
         <Scene key = "Inventry" component = {Inventry} title = "Inventory"  />
         <Scene key = "TruckerLogin" component = {TruckerLogin} title = "Trucker Login"  />
      </Scene>
   </Router>
)
export default Routes